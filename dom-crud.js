// Create a new <a> element containing the text "Buy Now!" 
// with an id of "cta" after the last <p>
const link = document.creatEkement('a');
link.innerText = 'Buy Now!';
link.id = 'cta';

const main = document.querySelector('main');
main.appendChild(link);

// Access (read) the data-color attribute of the <img>,
// log to the console
const imgEl = document.getElementsByTagName('img')[0];
console.log(imgEl.dataset.color);
// Update the third <li> item ("Turbocharged"), 
// set the class name to "highlight"
const featureEl = document.getElementById('features');
const li3 = featureEl.getElementsByTagName('li')([2];
li3.classList.add('highlight');


// Remove (delete) the last paragraph
// (starts with "Available for purchase now…")
const pElements = document.getElementsByTagName('p');
const p = pElements[pElements.length -1];
main.removeChild(p);
